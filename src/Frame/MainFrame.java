package Frame;

import Behavior.BehaviorObject;
import JSON.JSONArray;
import JSON.JSONObject;
import file.FileMananger;
import java.util.ArrayList;

/**
 *
 * @author rodrigo
 */
public class MainFrame extends javax.swing.JFrame {

    private static final int BASE_VALUE = 5;

    FileMananger fm;
    StringBuilder comportamento = new StringBuilder();
    ArrayList<BehaviorObject> dadosBase = new ArrayList<>();
    ArrayList<Integer> dados = new ArrayList<>();
    Graph graph;

    public MainFrame() {
        initComponents();
        fm = new FileMananger();
    }

    private void generate() {

        if (fm.openFile(comportamento)) {
            log("Sucesso ao ler arquivo");
            String sComportamento = comportamento.toString();
            JSONArray JSAcomportamento = new JSONArray(sComportamento);
            for (int i = 0; i < JSAcomportamento.length(); i++) {
                String stringObjetct = JSAcomportamento.get(i).toString();
                BehaviorObject bObj = new BehaviorObject();
                JSONObject js = new JSONObject(stringObjetct);

                bObj.setBase(js.getInt("base"));
                bObj.setMinutes(js.getInt("minutes"));
                bObj.setDirection(js.getInt("direction"));
                bObj.setMaxLimit(js.getInt("maxLimit"));
                bObj.setMinLimit(js.getInt("minLimit"));
                dadosBase.add(bObj);
            }
        }

        int valorAnterior = 70;
        for (int i = 0; i < dadosBase.size(); i++) {
            int base = dadosBase.get(i).getBase();
            int minutes = dadosBase.get(i).getMinutes();
            int direcao = dadosBase.get(i).getDirection();
            int minimo = dadosBase.get(i).getMinLimit();
            int maximo = dadosBase.get(i).getMaxLimit();

            int qnt = minutes / BASE_VALUE;
            qnt = (qnt < 1) ? 1 : qnt;

            for (int j = 0; j < qnt; j++) {
                int qntRestanteTemperatura = 0;
                int amplitude = (maximo - minimo) / 5;
                amplitude = (amplitude < 1) ? 1 : amplitude;
                int valorAtual = 0;

                if (direcao == -1) {
                    qntRestanteTemperatura = valorAnterior - minimo;
                    qntRestanteTemperatura = (qntRestanteTemperatura < 1) ? 1 : qntRestanteTemperatura;
                    int variacao = (int) (Math.random() * amplitude) % qntRestanteTemperatura;

                    if (qntRestanteTemperatura < 2) {
                        int variacaoExt = (int) (Math.random() * 10) % 5;
                        if (variacaoExt == 1) {
                            variacao++;
                        }
                        if (variacaoExt == 2 || variacaoExt == 4) {
                            variacao--;
                        }
                    }

                    valorAtual = valorAnterior - variacao;

                    log("Diminui\t valor " + valorAtual + "\tRestante " + qntRestanteTemperatura + "\tvariacao " + variacao + "\tamplitude " + amplitude);

                } else {
                    qntRestanteTemperatura = maximo - valorAnterior;
                    qntRestanteTemperatura = (qntRestanteTemperatura < 1) ? 1 : qntRestanteTemperatura;
                    int variacao = (int) (Math.random() * amplitude) % qntRestanteTemperatura;
                    if (qntRestanteTemperatura < 2) {
                        int variacaoExt = (int) (Math.random() * 10) % 5;
                        if (variacaoExt == 1) {
                            variacao++;
                        }
                        if (variacaoExt == 2 || variacaoExt == 4) {
                            variacao--;
                        }
                    }

                    valorAtual = valorAnterior + variacao;
                    log("Aumenta\t valor " + valorAtual + "\tRestante " + qntRestanteTemperatura + "\tvariacao " + variacao + "\tamplitude " + amplitude);
                }

                dados.add(valorAtual);
                valorAnterior = valorAtual;
            }
        }

        for (int i = 0; i < dados.size(); i++) {
            data(dados.get(i));
        }

        graph.setData(dados);
        graph.setVisible(true);
    }

    private void log(String l) {
        jTLog.append(l + "\n");
    }

    private void data(int l) {
        jTData.append(l + "\n");
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTLog = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTData = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("LOGs"));

        jTLog.setEditable(false);
        jTLog.setColumns(20);
        jTLog.setRows(5);
        jScrollPane1.setViewportView(jTLog);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados"));

        jTData.setEditable(false);
        jTData.setColumns(20);
        jTData.setRows(5);
        jScrollPane2.setViewportView(jTData);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 686, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jMenu1.setText("Arquivos");

        jMenuItem1.setText("Carregar regras");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        generate();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTData;
    private javax.swing.JTextArea jTLog;
    // End of variables declaration//GEN-END:variables

}
