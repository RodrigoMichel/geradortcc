package file;

import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileMananger {

    public boolean openFile(StringBuilder content) {

        try {
            JFileChooser fileChooser = new JFileChooser();
            if (fileChooser.showDialog(null, "Abrir") == JFileChooser.APPROVE_OPTION) {
                String fileName = fileChooser.getSelectedFile().getPath();
                System.out.println("File: " + fileName);

                BufferedReader br = new BufferedReader(new FileReader(fileName));
                while (br.ready()) {
                    content.append(br.readLine());
                }
                br.close();
            };
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Falha ao abrir arquivo\n\n" + e.getMessage(), "Falha", JOptionPane.WARNING_MESSAGE);
            return false;
        }

        return true;
    }
}
